Source: lomiri
Section: x11
Priority: optional
Maintainer: Lomiri Developers <lomiri-devel-discuss@lists.lomiri.com>
Build-Depends:
 cmake,
 cmake-extras,
 dbus-test-runner <!nocheck>,
 debhelper (>= 9),
 dh-apparmor,
 dh-migrations:all,
 dh-python,
 doxyqml <!cross>,
 g++:native,
 gdb <!cross>,
 graphviz <!cross>,
 lomiri-schemas (>= 0.1.3),
 libdeviceinfo-dev,
 libevdev-dev,
 libgeonames-dev (>= 0.2),
 libgl1-mesa-dev[!arm64 !armhf] | libgl-dev[!arm64 !armhf],
 libgl1-mesa-dri <!nocheck>,
 libgles2-mesa-dev[arm64 armhf],
 libglib2.0-dev,
 libgnome-desktop-3-dev,
 libgsettings-qt-dev,
 liblightdm-qt5-3-dev,
 liblomiri-connectivity-qt1-dev,
 libpam0g-dev,
 libpulse-dev,
 libqmenumodel-dev (>= 0.2.12),
 libqtmirserver-dev (>= 0.6.0),
 libqt5sql5-sqlite <!nocheck>,
 libqt5svg5-dev,
 libqt5xmlpatterns5-dev,
 libqtdbusmock1-dev (>= 0.7) <!nocheck>,
 libqtdbustest1-dev <!nocheck>,
 libsystemd-dev [linux-any],
 liblomirisystemsettings-dev,
 liblomiri-app-launch-dev (>= 0.1.5),
 liblomiri-download-manager-common-dev,
 liblomirigestures5-dev (>= 1.3.2030),
 liblomirigestures5-private-dev (>= 1.3.2030),
 libudev-dev,
 libldm-common-dev,
 liblomiri-api-dev,
 libusermetricsoutput1-dev,
 libx11-dev[!arm64 !armhf],
 libxcb1-dev[!arm64 !armhf],
 libxi-dev[!arm64 !armhf],
 mirtest-dev,
 pkg-config,
 python3-all:any <!nocheck>,
 python3-setuptools <!nocheck>,
 qml-module-qt-labs-folderlistmodel <!nocheck>,
 qml-module-qt-labs-settings <!nocheck>,
 qml-module-qtmultimedia (>= 5.6) <!nocheck>,
 qml-module-qtqml-statemachine <!nocheck>,
 qml-module-qtquick-layouts <!nocheck>,
 qml-module-qtquick-xmllistmodel <!nocheck>,
 qml-module-qtquick2 <!nocheck>,
 qml-module-qttest <!nocheck>,
 qml-module-lomiri-components (>= 1.3.2030) <!nocheck> | qml-module-lomiri-components-gles (>= 1.3.2030) <!nocheck>,
 qml-module-lomiri-layouts <!nocheck>,
 qml-module-lomiri-settings-components <!nocheck>,
 qml-module-lomiri-settings-menus <!nocheck>,
 qml-module-lomiri-test <!nocheck>,
 qtbase5-dev (>= 5.6),
 qtbase5-dev-tools,
 qtbase5-private-dev (>= 5.6),
 qtdbustest-runner <!nocheck>,
 qtdeclarative5-dev (>= 5.6),
 qtdeclarative5-dev-tools,
 qtdeclarative5-private-dev (>= 5.6),
 qml-module-lomiri-content,
 systemd [linux-any],
 ttf-ubuntu-font-family <!nocheck>,
 ubports-wallpapers <!nocheck>,
 xvfb <!nocheck>,
# mirtest pkgconfig requires these, but doesn't have a deb dependency. Bug lp:1633537
 libboost-filesystem-dev,
 libboost-system-dev,
Standards-Version: 3.9.4
Homepage: https://github.com/ubports/lomiri
Vcs-Git: https://github.com/ubports/lomiri
XS-Testsuite: autopkgtest
X-Lomiri-Use-Langpack: yes

Package: indicators-client
Architecture: any
Depends:
 qml-module-lomiri-components (>= 1.3.2030) | qml-module-lomiri-components-gles (>= 1.3.2030),
 qml-module-qmenumodel1,
 lomiri (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Indicators client test application
 This package contains the client application for indicators used by autopilot

Package: lomiri-greeter
Architecture: any
Depends:
 lomiri-system-compositor,
 lomiri (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 lightdm,
Provides:
 lightdm-greeter,
Description: The Lomiri Greeter
 The Lomiri greeter is the primary login greeter for Lomiri devices.

Package: lomiri
Architecture: any
Provides:
 indicator-renderer,
 notification-daemon
Depends:
 dmz-cursor-theme,
 gsettings-desktop-schemas,
 libcap2-bin,
 libglib2.0-bin,
 qml-module-biometryd,
 qml-module-lomiri-components,
 qml-module-lomiri-layouts,
 qml-module-lomiri-settings-menus,
 qml-module-qmenumodel1 (>= 0.9.1-1ubports2~),
 qml-module-qt-labs-folderlistmodel,
 qml-module-qt-labs-settings,
 qml-module-qtmir (>= 0.4.8),
 qml-module-qtqml-statemachine,
 qml-module-qtquick-xmllistmodel,
 qml-module-hfd,
 qml-module-lomiri-telephony0.1,
 qtmir,
 lomiri-system-settings,
 lomiri-launcher-impl-13,
 lomiri-common (= ${source:Version}),
 lomiri-private (= ${binary:Version}),
 lomiri-private | lomiri-launcher-impl,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ayatana-indicator-keyboard,
 ayatana-indicator-session,
 lomiri-app-launch (>= 0.1.5),
 lomiri-greeter-session-broadcast,
Breaks:
 indicator-network (<< 0.5.1+14.10.20141014),
 lomiri-keyboard (<< 0.100),
 lomiri-touch-session (<< 0.107),
 lomiri-scope-tool,
Replaces:
 lomiri-touch-session (<< 0.82~),
 lomiri-scope-tool,
Conflicts:
 lomiri-system-settings-wizard,
Description: Lomiri shell
 The Lomiri shell is the primary user interface for Lomiri devices.

Package: lomiri-common
Architecture: all
Depends:
 qml-module-qtquick-layouts,
 qml-module-lomiri-components (>= 1.3.2030) | qml-module-lomiri-components-gles (>= 1.3.2030),
 qml-module-lomiri-settings-components,
 qml-module-lomiri-thumbnailer0.1,
 qtdeclarative5-lomiri-notifications-plugin (>= 0.1.2) | lomiri-notifications-impl,
 qttranslations5-l10n,
 lomiri-application-impl-28,
 lomiri-notifications-impl-3,
 lomiri-schemas,
 lomiri-tests | lomiri-application-impl,
 ${misc:Depends},
Description: Lomiri shell (common files)
 The Lomiri shell is the primary user interface for Lomiri devices.
 .
 This package contains the QML, graphics and locale files for lomiri.

Package: lomiri-autopilot
Architecture: all
Build-Profiles: <!cross>
Depends:
 autopilot-qt5 (>= 1.4),
 gir1.2-glib-2.0,
 gir1.2-notify-0.7,
 libqt5test5,
 libqt5widgets5,
 ofono-phonesim,
 python3-autopilot,
 python3-evdev,
 python3-fixtures,
 python3-gi,
 qttestability-autopilot (>= 1.4),
 lomiri-ui-toolkit-autopilot (>= 1.3.2030),
 lomiri (= ${source:Version}),
 lomiri-tests (= ${source:Version}),
 url-dispatcher-tools,
 xvfb,
 ${misc:Depends},
 ${python3:Depends},
Description: Test package for Lomiri shell
 Autopilot tests for the lomiri package

Package: lomiri-tests
Architecture: any
Multi-Arch: same
Build-Profiles: <!cross>
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 dbus-test-runner,
 parallel,
 qt5-default,
 qtdbustest-runner,
 qtdeclarative5-dev-tools,
 lomiri (= ${source:Version}),
 xvfb,
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 lomiri-application-impl,
 lomiri-application-impl-28,
 lomiri-fake-env,
Replaces:
 lomiri-autopilot (<< 8.02+15.04.20150422-0lomiri1),
 lomiri-fake-env,
Conflicts:
 lomiri-fake-env,
Description: Scripts and mocks for running Lomiri shell tests
 Provides fake implementations of some QML modules used by Lomiri shell
 (e.g Lomiri.Application) so that you can run it in a sandboxed environment
 and scripts to run its test suite.

Package: lomiri-private
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 accountsservice-ubuntu-schemas (>= 0.0.7),
 lomiri-schemas,
 qml-module-gsettings1.0,
 qml-module-qtmultimedia,
 lomiri-schemas,
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 lomiri-launcher-impl,
 lomiri-launcher-impl-13,
Description: Lomiri private libs
 The Lomiri shell is the primary user interface for Lomiri devices.
 .
 This package contains the private libraries for QML and other components
 only used by the shell.

Package: lomiri-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!cross>
Depends:
 ${misc:Depends},
Description: Documentation for Lomiri
 The Lomiri shell is the primary user interface for Lomiri devices. (documentation)
